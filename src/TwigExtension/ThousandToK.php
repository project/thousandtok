<?php

namespace Drupal\thousandtok\TwigExtension;

/**
 * Class ThousandToK to provide twig extension.
 *
 * @package Drupal\thousandtok\TwigExtension
 */
class ThousandToK extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [new \Twig_SimpleFilter('thousandtok', [$this, 'process'])];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'thousandtok.twig_extension';
  }

  /**
   * Process the string.
   *
   * @param string $string
   *   String.
   *
   * @return mixed
   *   Formatted.
   */
  public static function process($string) {
    return short_scale_formatter_format_number(floatval($string), [], 0);
  }

}
