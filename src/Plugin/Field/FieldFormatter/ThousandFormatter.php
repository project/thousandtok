<?php

namespace Drupal\thousandtok\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\NumericFormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'number_integer' formatter.
 *
 * The 'Default' formatter is different for integer fields on the one hand, and
 * for decimal and float fields on the other hand, in order to be able to use
 * different settings.
 *
 * @FieldFormatter(
 *   id = "number_thousand",
 *   label = @Translation("Thousand to k"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class ThousandFormatter extends NumericFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'thousand_separator' => '',
      'prefix_suffix' => TRUE,
      'thousand' => 'k',
      'million' => 'm',
      'billion' => 'b',
      'trillion' => 't',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function numberFormat($number) {
    $characters = [
      'thousand' => $this->getSetting('thousand'),
      'million' => $this->getSetting('million'),
      'billion' => $this->getSetting('billion'),
      'trillion' => $this->getSetting('trillion'),
    ];
    return short_scale_formatter_format_number($number, $characters);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements['prefix_suffix'] = [
      '#type' => 'checkbox',
      '#title' => t('Display prefix and suffix'),
      '#default_value' => $this->getSetting('prefix_suffix'),
      '#weight' => 10,
    ];
    $elements['thousand'] = [
      '#type' => 'textfield',
      '#title' => t('Thousand character'),
      '#default_value' => $this->getSetting('thousand'),
      '#weight' => 10,
    ];
    $elements['million'] = [
      '#type' => 'textfield',
      '#title' => t('Million character'),
      '#default_value' => $this->getSetting('million'),
      '#weight' => 10,
    ];
    $elements['billion'] = [
      '#type' => 'textfield',
      '#title' => t('Billion character'),
      '#default_value' => $this->getSetting('billion'),
      '#weight' => 10,
    ];
    $elements['trillion'] = [
      '#type' => 'textfield',
      '#title' => t('Trillion character'),
      '#default_value' => $this->getSetting('trillion'),
      '#weight' => 10,
    ];

    return $elements;
  }

}
