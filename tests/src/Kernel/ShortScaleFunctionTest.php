<?php

namespace Drupal\tests\thousandtok\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test class for short scale function.
 */
class ShortScaleFunctionTest extends KernelTestBase {

  public static $modules = ['thousandtok'];

  public function testShortScaleFunctionNumberFormat() {
    $this->assertEquals(1, short_scale_formatter_format_number(1));
    $this->assertEquals('1k', short_scale_formatter_format_number(1000));
    $this->assertEquals('101k', short_scale_formatter_format_number(101800));
  }

  public function testMilionNumber() {
    $this->assertEquals('2m', short_scale_formatter_format_number(2000000));
    $this->assertEquals('7.8m', short_scale_formatter_format_number(7800000));
    $this->assertEquals('92m', short_scale_formatter_format_number(92150000));
    $this->assertEquals('123m', short_scale_formatter_format_number(123200000));
    $this->assertEquals('9.9m', short_scale_formatter_format_number(9999999));
  }

  public function testBillionNumber() {
    $this->assertEquals('2.6b', short_scale_formatter_format_number(2600000000));
  }

  public function testTrillionNumber() {
    $this->assertEquals('1.1t', short_scale_formatter_format_number(1135060000000));
  }

  public function testChangeFormatText() {
    $chars = [
      'thousand' => 'nghin',
      'million' => 'trieu',
      'billion' => 'ty',
      'trillion' => 'nghin ty',
    ];

    $this->assertEquals('1nghin', short_scale_formatter_format_number(1000, $chars));
    $this->assertEquals('10nghin', short_scale_formatter_format_number(10000, $chars));
    $this->assertEquals('1trieu', short_scale_formatter_format_number(1000000, $chars));
    $this->assertEquals('1.1nghin ty', short_scale_formatter_format_number(1135060000000, $chars));

  }

}
