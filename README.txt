CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Maintainers

Introduction
------------

This module provide formatter to convert 1000 to k.


Installation
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


Maintainers
-----------

 * Lap Pham (phthlaap) - https://www.drupal.org/user/3579545
